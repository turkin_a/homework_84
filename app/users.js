const express = require('express');
const nanoid = require('nanoid');
const bcrypt = require('bcrypt');

const User = require('../models/User');

const router = express.Router();

const createRouter = () => {
  router.post('/', (req, res) => {
    const userData = req.body;
    userData.token = nanoid();

    const user = new User(userData);

    user.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  router.post('/sessions', async (req, res) => {
    const user = await User.findOne({username: req.body.username});

    if (!user) {
      return res.status(400).send({error: 'User not found'});
    }

    const isMatch = await bcrypt.compare(req.body.password, user.password);

    if (!isMatch) {
      return res.status(400).send({error: 'Password is wrong!'});
    }

    user.token = nanoid();
    user.save();

    return res.send({token: user.token});
  });

  return router;
};

module.exports = createRouter;