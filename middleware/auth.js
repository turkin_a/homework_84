const User = require('../models/User');

const auth = async (req, res, next) => {
  const token = req.get('Token');

  if (!token) {
    res.status(401).send({error: 'Token not present'});
  }

  const user = await User.findOne({token});

  if (!user) {
    res.status(401).send({error: 'Token incorrect'});
  }

  req.user = user;

  next();
};

module.exports = auth;